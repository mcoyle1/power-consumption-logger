#include <Wire.h>
#include <LCD.h>
#include <LiquidCrystal_I2C.h>
#include <SPI.h>
#include <SD.h>
#include <string.h>

//Sensors and Switches 
#define BtnPin 2 // Start/Stop button
#define LCDBtnPin 3 // Backlight On/Off button
#define VoltagePin 0
#define CurrentPin 1 //Sensor Pins
#define NumSensorSamples 2000 // Number to times to super-sample ADC
#define VoltagePrecision 2 // Number of decimal places to round voltage reading to
#define CurrentPrecision 3 // Number of decimal places to round current reading to
int TestNumber = 0, i = 0;
volatile int BtnValue = 0; // ISR var
float vSum = 0.0, aSum = 0.0; // Sensor reading sums
float vOffset = 0.0, aOffset = 0.0; // Default voltage and current offsets
float Voltage = 0, Current = 0; // Sensor Values
long Clock = 0, StartTimer = 0, StopTimer = 0; // Counters/Timers

//LCD
#define I2C_ADDR 0x27 // Define I2C Address where the SainSmart LCD is
#define BACKLIGHT_PIN 3
#define ScreenDelay 1000
LiquidCrystal_I2C lcd(I2C_ADDR, 2, 1, 0, 4, 5, 6, 7);
String LCDData = "";

//SD Card
#define MAX_NUMBER_OF_FILES 4 //Does this have to be a #define? Couldn't we use const?
#define TRAP false // Set this to true/false to indicate whether to allow logging with
                   // more than MAX_NUMBER_OF_FILES on the SD card
#define chipSelect 10 // arduino pin for SD card
#define LogNamePrefix "log"
#define LogFileExtension ".csv"
#define LogFileHeader "Time,Voltage,Current"
#define FrameInterval 1000 // Time in ms between readings
String DataString = "", FileName = "";
File dataFile;

void setup()
{
  //Switches Mode
  pinMode(BtnPin, INPUT_PULLUP);
  pinMode(LCDBtnPin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(BtnPin), StartStopMeasurement, LOW);

  //LCD SETUP
  lcd.begin (20, 4);
  lcd.setBacklightPin(3, POSITIVE);
  lcd.setBacklight(HIGH);
  lcd.home();
  
  //SD Card
  LCDWrite(0, 0, "Initializing SD Card");
  delay(ScreenDelay);
  
  //Checking if SD Card is inserted
  while (!SD.begin(chipSelect))
    LCDWrite(0, 1, "SD Card Failed");
  
  //SD card is Inserted
  LCDWrite(0, 1, "SD Card Initialized");
  delay(ScreenDelay);
  
  //Read file names from SD card to determine last log number
  while (SD.exists(LogNamePrefix + String(TestNumber) + LogFileExtension))
  {
    TestNumber++;
    
    if(TestNumber >= MAX_NUMBER_OF_FILES && TRAP)
    {
      while (true)
      {
        LCDWrite(0, 2, "File System Full!");
        delay(ScreenDelay);
      }
    }
  }
  LCDWrite(0, 2, String(TestNumber) + " logs on SD!");
  delay(ScreenDelay);
  
  // Calibrate sensors for current 3D positioning
  LCDWrite(0, 3, "Calibrating");
  for (i = 0; i < NumSensorSamples; i++) 
  {
    vSum -= (analogRead(VoltagePin) / 40.92);
    aSum -= ((((analogRead(CurrentPin) * 5.0) / 1024.0) - 2.5) / 0.100);
  }
  vOffset = vSum / NumSensorSamples;
  aOffset = aSum / NumSensorSamples;
  ClearSensorAveragingSums();
  
  // Clear screen and advance to loop
  CLS();
}

void loop()
{
  LCDWrite(0, 0, "System Ready");
  
  //Adjust backlight based on switch position
  ChangeLCDBacklight();

  //Start measurements when push button is actuated
  while (BtnValue == 1)
  {
    if(Clock == 0) 
    {
      //Create a new csv file starting with log0
      FileName = LogNamePrefix + String(TestNumber) + LogFileExtension;

      //Open card for writing
      dataFile = SD.open(FileName, FILE_WRITE);
    }

    //Measure loop time to offset delay amount
    StartTimer = millis();
    
    //Adjust backlight based on switch position while logging
    ChangeLCDBacklight();
    
    // Reset the sensor averaging sums
    ClearSensorAveragingSums();
  
    // Take measurements from sensors
    for(i = 0; i < NumSensorSamples; i++)
    {
      vSum += (analogRead(VoltagePin) / 40.92);
      aSum += ((((analogRead(CurrentPin) * 5.0) / 1024.0) - 2.5) / 0.100);
    }
    
    //Record Measurements from Sensors
    Voltage = (vSum/NumSensorSamples) + vOffset; // Convert ADC to value, offset with learned error
    Current = (aSum/NumSensorSamples) + aOffset;  // Convert ADC to value, offset with learned error

    //Round values to required precision
    Voltage = round(Voltage * pow(10, VoltagePrecision)) / pow(10, VoltagePrecision);
    Current = round(Current * pow(10, CurrentPrecision)) / pow(10, CurrentPrecision);
    
    //On first iteration of logging loop
    if (Clock == 0) 
    {
      //Show logging status
      LCDWrite(0, 0, "Logging Data");
      
      //Write csv header to file
      dataFile.println(LogFileHeader);
      dataFile.flush();
    }
    
    //Generate log tick string
    DataString = String(Clock) + "," + String(Voltage, VoltagePrecision) + "," + String(Current, CurrentPrecision);

    //Write string to file and close file to prevent file corruption if power is removed
    dataFile.println(DataString);
    dataFile.flush();
    
    //Print data to LCD every tick
    LCDData = "Read:" + String(Voltage, VoltagePrecision) + "V, " + String(Current, CurrentPrecision) + "A"; //Print Sensor Data to LCD
    LCDWrite(0, 1, LCDData);
    
    //Increment time on LCD every tick
    LCDWrite(0, 2, "Time:" + String(Clock/3600) + "h, " + String((Clock/60)%60) + "m, " + String(Clock%60) + "s");
    
    //Show current filename on LCD every tick
    LCDWrite(0, 3, "File:" + FileName);
    
    //Wait for next iteration
    StopTimer = millis();
    delay(FrameInterval - (StopTimer - StartTimer)); // Delay until time for next reading
    Clock++;
    
    //Stop logging if start/stop button has been detected by ISR
    if (BtnValue == 0) 
    {
      CLS();
      LCDWrite(0, 0, "Storing Data");
      dataFile.close();
      TestNumber++;
      delay(ScreenDelay);
      LCDWrite(0, 1, FileName);
      Clock = 0;
      CLS();
      break;
    }
  } // while
} // loop

//Function : Set LCD Cursor Position and Write Message
void LCDWrite(int c, int r, String message)
{
  while(message.length() < 20){ message += " "; }
  lcd.setCursor (c,r);
  lcd.print(message);
}

//Function : Interrupt and Stop Data Measurements (sets global flag)
void StartStopMeasurement()
{
  if (Clock >= 1)
    BtnValue = 0;
  else
    BtnValue = 1;
}

//Function : Update backlight state
void ChangeLCDBacklight()
{
  lcd.setBacklight(!digitalRead(LCDBtnPin));
}

//Function : Reset/Clear sensor averaging sums
void ClearSensorAveragingSums()
{
  vSum = 0;
  aSum = 0;
}

//Function : Wait and clear the screen
void CLS()
{
  delay(ScreenDelay);
  lcd.clear();
}
